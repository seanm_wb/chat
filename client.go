package main

import (
	"github.com/gorilla/websocket"
)

// client represents a single chatting user
type client struct {
	socket *websocket.Conn
	send   chan []byte
	room   *room
}

/*
   Allows our user to read from the websocket, continually sending messages
   to the "forward" channel for the room.
*/
func (c *client) read() {
	for {
		if _, msg, err := c.socket.ReadMessage(); err == nil {
			c.room.forward <- msg
		} else {
			break
		}
	}
	c.socket.Close()
}

/*
   continually gets messages from the "send" channel, and writes them
*/
func (c *client) write() {
	for msg := range c.send {
		if err := c.socket.WriteMessage(websocket.TextMessage, msg); err != nil {
			break
		}
	}
	c.socket.Close()
}
