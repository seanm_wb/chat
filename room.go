package main

import (
	"log"
	"net/http"

	"github.com/gorilla/websocket"
)

type room struct {
	// forward is a channel that holds incoming messages for other users
	// in that channel
	forward chan []byte
	// join is a channel for clients that want to join the room
	join chan *client
	// leave is a channel for clients who want to leave the room
	leave chan *client
	// clients hold all the current clients of the room
	clients map[*client]bool
}

// newRoom makes a new room so you don't have to know what is required
func newRoom() *room {
	return &room{
		forward: make(chan []byte),
		join:    make(chan *client),
		leave:   make(chan *client),
		clients: make(map[*client]bool),
	}
}

/*
   Run forever. Check each channel for updates.
   If we get a message on the join channel, update our room clients map
   If we get a message on the leave channel, remove the user from the room clients map
   If we get a message on the forward channel, iterate over all users, and send the messages
   to their send channel. From there, write() will send it to the browser over the websocket
   If the send channel is closed, we know the user is not connected to this room,
   so tidy up our room map
*/
func (r *room) run() {
	for {
		select {
		case client := <-r.join:
			// joining
			r.clients[client] = true
		case client := <-r.leave:
			// leaving
			delete(r.clients, client)
			close(client.send)
		case msg := <-r.forward:
			// forward message to all clients
			for client := range r.clients {
				select {
				case client.send <- msg:
					//send the messages
				default:
					// failed to send
					delete(r.clients, client)
					close(client.send)
				}
			}
		}
	}
}

const (
	socketBufferSize  = 1024
	messageBufferSize = 256
)

// allows us to use websockets
var upgrader = &websocket.Upgrader{ReadBufferSize: socketBufferSize,
	WriteBufferSize: socketBufferSize}

/*
   HTTP handler to allow users to enter or leave a room
*/
func (r *room) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	// gets the websocket from the request
	socket, err := upgrader.Upgrade(w, req, nil)
	if err != nil {
		log.Fatal("serveHTTP:", err)
		return
	}
	/*
	   preceding code creates our client information, and adds it
	   to the join channel queue of the room they want to join
	*/
	client := &client{
		socket: socket,
		send:   make(chan []byte, messageBufferSize),
		room:   r,
	}
	r.join <- client
	defer func() { r.leave <- client }()

	go client.write()
	client.read()
}
